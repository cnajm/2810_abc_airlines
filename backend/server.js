const express = require('express')
const sqlite3 = require('better-sqlite3')
const colors = require('colors') // console colors
const bcrypt = require('bcryptjs');
const queryString = require('query-string');
const { encrypt, decrypt } = require('./encryption');

// sqlite3.verbose() //https://github.com/mapbox/node-sqlite3/wiki/API#sqlite3verbose

const app = express()
var db = false

function dbConnect() {
	db = new sqlite3('../abcAirlines.db', {fileMustExist: true, verbose: console.log })
	console.log(db);
	if (!db) {		
		console.log("unable to connect");
	}
	else {
		console.log('connected to the project database.');
	}
}

let validatePass = (pass, hash) => {return bcrypt.compareSync(pass, hash)}
let genHash = seed => {return bcrypt.hashSync(seed, bcrypt.genSaltSync(10), null)}

function fetchUser(username) {
	let sql = db.prepare('SELECT * FROM Users WHERE name=?')
	return sql.get(username)
}

function fetchLoggedIn(token) {
	let sql = db.prepare('SELECT * FROM LoggedIn WHERE token=?')
	return sql.get(token)
}

function insertLoggedIn(uid, token) {
	// replace the value if one already exists
	let sql = db.prepare('INSERT OR REPLACE INTO LoggedIn (uid, token) VALUES (?, ?)')
	const q = sql.run(uid, token)
	return q.changes
}

function deleteLoggedIn(token) {
	let sql = db.prepare('DELETE FROM LoggedIn WHERE token=(?)')
	const q = sql.run(token)
	return q.changes
}

function fetchEmployeeData() {
	let sql = db.prepare('SELECT id, name, role FROM Users')
	return sql.all()
}

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Credentials', true)
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
	next()
});

function logTime(req, res, next) { // logs time and route details with colors
  let now = new Date()
  let status = res.statusCode;
  status = status == 200 ? colors.green(status) : colors.brightRed(status)
  console.log('Time:'.brightRed, now.toUTCString().yellow)
  console.log(`${colors.yellow(req.method)}:${colors.brightMagenta(req.url)} ${status}`);
  next()
}

function decryptionSequence(req) {
	let hash = {}
	// hash.iv = req.query.iv
	hash.content = req.query.content
	console.log('Received encrypted data', colors.brightRed(hash.content));
	const d = decrypt(hash)
	let data = queryString.parse(d)
	data = JSON.parse(JSON.stringify(data))
	console.log('Decrypted', colors.brightGreen(data));
	return data
}

// Routes

// default logger middleware
app.get('/*', logTime);

app.get('/connect', (req, res) => {
	let out
	db = new sqlite3.Database('../project.db', sqlite3.OPEN_READWRITE, (err) => {
	if (err) {
		console.error(err.message);
		out = "NOPE"
	}
	else {
		console.log('connected to the project database.');
		out = "YEP HELLO"
	}
	res.send(out)
	});
})
app.get('/close', (req, res) => {
	// close the database connection
	console.log('YEP closing db');
	db.close();
	res.send('CLOSED')
})

app.get('/login', (req, res) => {
	const data = decryptionSequence(req)

	const username = data.username;
	const password = data.password;

	const user = fetchUser(username)
	if(validatePass(password, user.pass)) {
		let now = new Date()
		let token = ''+Math.floor(Math.random() * (1000000000))//genHash(`${username}${now.toUTCString()}${username}`)
		insertLoggedIn(user.id, token)
		return res.status(200).send({...user, token: token})
	}
	else {
		return res.status(401).send({message: 'Invalid credentials'})
	}
})

app.get('/logout', (req, res) => {
	const token = req.query.token;
	deleteLoggedIn(token)
	return res.status(200).send({message: 'Successfully logged out'})
})

app.get('/authed', (req, res) => {
	const data = decryptionSequence(req)

	const token = data.token;
	let out = fetchLoggedIn(token)
	return res.status(200).send(token == out.token)
})

app.get('/GetEmployeeData', (req, res) => {
	let data = fetchEmployeeData()
	let hash = encrypt(JSON.stringify(data))
	return res.status(200).send(hash)
})

dbConnect()
app.listen(8000, () => console.log('server running on port 8000'));
