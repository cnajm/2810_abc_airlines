BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Users" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL,
	"pass"	TEXT,
	"role"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "LoggedIn" (
	"uid"	INTEGER UNIQUE,
	"token"	TEXT,
	FOREIGN KEY("uid") REFERENCES "Users"("id")
);
INSERT INTO "Users" ("id","name","pass","role") VALUES (1,'Ceasar','$2a$10$DhSfx0/VrI0HcFlx4aQLvuRIQiXsm9WGX.M2hkXH6me.MTimjsJiG','ceo'),
 (2,'Mark','$2a$10$eWnEqWHtpG5LdR6297e.nuq8TRepOYPLV1wMmnwgsGfpHNecwOm/y','manager'),
 (3,'Patrick','$2a$10$.zCBkiypQ5PZTFj0wITPcOMvzV5bBegBSCzWrVYsTGeBzo.A7XGLi','pilot'),
 (4,'Paige','$2a$10$FF4mXirsDuXmAWHfO278heU1Mha.EP1SgHRsHvuVIO6dDFzG4XvVe','pilot');
INSERT INTO "LoggedIn" ("uid","token") VALUES (4,'840740692'),
 (2,'212922678'),
 (1,'44893374');
COMMIT;
