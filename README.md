# 2810 12/3/20 100349302
- [Installation](#installation)
- [Frontend](#frontend)
- [Backend](#backend)
# Group Project (solo)
- SQL DB setup file included. The backend looks for the DB in the root directory.

## Installation 

This project uses the SEVN stack (SQL, Express, Vue, NodeJS) and is to be run off your localhost.

### Frontend
The two components share some dependencies. After the first install, the second should finish quicker due to cached modules. The frontend is heftier than the backend, it took around ~2 minutes to get it online on a 1 MB/s connection.
```
cd frontend/
npm install
npm run serve
```
### Backend
```
cd backend/
npm install
npm run start
```
Run both simultaneously in two terminal sessions then navigate to `localhost:8080`
