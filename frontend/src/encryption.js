const crypto = require('crypto')
const alg = 'aes-256-ctr'
const secret = 'YEPYEPYEPYEPYEPYEPYEPYEPYEPYEPYE'
const iv = 'NOPENOPENOPENOPE'

const encrypt = (text) => {
    const cipher = crypto.createCipheriv(alg, secret, iv)
    const e = Buffer.concat([cipher.update(text), cipher.final()])
    return {
        // iv: iv,
        content: e.toString('hex')
    }
}
const decrypt = (hash) => {
    const decipher = crypto.createDecipheriv(alg, secret, iv) // hash.iv
    const d = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()])
    return d.toString()
}

module.exports = { encrypt, decrypt }