import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
Vue.use(VueRouter)

const fetch = require('node-fetch');
const querystring = require('querystring');
const { encrypt } = require('@/encryption');

async function isLoggedIn() {
	// console.log(window);
	let sessionToken = Vue.cookie.get('stoken')
	console.log('TOKEN ',sessionToken);
	// check that a token exists
	if (!sessionToken) 
		return false
	// token exists, validate it through the server
	const query = querystring.stringify({token: sessionToken})
  let hash = encrypt(query)
  const encrQuery = querystring.stringify(hash)
	let res = await fetch(`http://localhost:8000/authed?${encrQuery}`)
	let json = await res.json()
	console.log(json)
	return json
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    const loggedIn = await isLoggedIn()
    console.log('status ', loggedIn)
    console.log('verifying auth');
    if (!loggedIn) {
      next({ name: 'Home', query: {unauthedLogin: 1}})
    } 
    else {
      next()
    }
  } 
  if (to.matched.some(record => record.meta.roleAccess)) {
    // check if logged in user has the required role to view the route
    // if not, redirect to profile page.
    const user = JSON.parse(localStorage.getItem('user'))

    if (!to.meta.roleAccess.includes(user.role)) {
      next({ name: 'Profile', query: {unauthedRole: 1}})
    } 
    else {
      next()
    }
  } 
  else {
    next() // make sure to always call next()
  }

  if (to.matched.some(record => record.meta.title)) {
    document.title = to.meta.title;
  } 
  else {
    document.title = 'proj_2810_front';
  }
})

export default router
