import HomeView from '../views/Home.vue'
import NotFoundView from '../views/404.vue'
import FlightsView from '../views/Flights.vue'
import InfoView from '../views/About.vue'
import ProfileView from '../views/Profile.vue'
import EmployeesView from '../views/Employees.vue'
import PerformanceView from '../views/Performance.vue'
export default [
  {
    name: 'Home',
    path: '/',
    alias: '/login',
    component: HomeView,
  },
  {
    name: 'Info',
    path: '/about',
    component: InfoView
  },
  { name: 'Profile',
    path: '/profile',
    component: ProfileView
  },
  {
    name: 'Flights',
    path: '/flights',
    component: FlightsView,
    meta: {
      title: 'Flights',
      requiresAuth: true,
    }
  },
  {
    name: 'Employees',
    path: '/employees',
    component: EmployeesView,
    meta: {
      title: 'Employees',
      requiresAuth: true,
      roleAccess: ['ceo', 'manager']
    }
  },
  {
    name: 'Company Performance',
    path: '/performance',
    component: PerformanceView,
    meta: {
      title: 'CEO: Company Performance',
      requiresAuth: true,
      roleAccess: ['ceo']
    }
  },
  {
    path: '*',
    component: NotFoundView
  }
]
