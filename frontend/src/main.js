import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueCookie from 'vue-cookie';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import router from './router'

// Vue.config.productionTip = false
Vue.config.performance = true

Vue.use(VueCookie)

new Vue({
  vuetify,
  router,
  render: h => h(App),
}).$mount('#app')
